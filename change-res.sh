#!/bin/bash
clear
echo
echo
echo
echo
echo
echo "############################################################"
echo "#  Please add resolution behind file name if not done so.  #" 
echo "# eg. - sh change-res.sh footage-1-a.mp4 640:480           #"
echo "############################################################"
echo
echo
echo
echo



FILE_RES=$1
RES=$2
if [ -f "$FILE_RES" ]; then
    echo "$FILE_RES exists."
else 
    clear
    echo "Fatal error"
    echo "File $FILE_RES does not exist."
    echo ""
    echo "Please start script with needed Videofile e.g."
    echo "################################################"
    echo "#  sh change-fps.sh footage-1-b.mp4 <FPS>   #" 
    echo "################################################"
    echo ""
    echo "if you haven't downloaded the footage yet, download it first with WGET."
    echo ""
    exit
fi
    echo "$FILE_FPS exists."
echo $FPS

#Remove '*.mp4' for new filename
VIDEO_NAME=${FILE_RES%%.*}
echo $VIDEO_NAME
echo $RES
NAME_AND_RES="${VIDEO_NAME}_${RES}RES"
NAME_RES_AND_TEXT="${NAME_AND_RES}_withTXT"
NEW_FILE_NAME="${VIDEO_NAME}_${RES}"

#########
echo $NAME_AND_RES
echo $NAME_RES_AND_TEXT
#########

docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg -y -vsync 0 -hwaccel cuda -hwaccel_output_format cuda -i "$FILE_RES" -vf scale_cuda=${RES} -g 10 -an -c:v h264_nvenc -b:v 2M "$NAME_AND_RES".mp4
#docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg 
ffmpeg -i "$NAME_AND_RES".mp4 -vf "drawtext=text='$NAME_AND_RES':x=10:y=40:fontsize=20:fontcolor=red" -g 10 -an -b:v 2M "$NEW_FILE_NAME".mp4
rm -f "$NAME_AND_RES".mp4

ls -l -h *.mp4 *.MP4