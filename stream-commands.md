#default command:
ffmpeg -re -stream_loop -1 -i ${VIDEO_FILE} -c copy -f rtsp rtsp://localhost:8554/compose-rtsp

#with file:
ffmpeg -re -stream_loop -1 -i footage-1-a.mp4 -c copy -f rtsp rtsp://localhost:8554/compose-rtsp

#test with text center:
ffmpeg -re -stream_loop -1 -i footage-1-a.mp4 -vf "drawtext=text='Centered Text':x=(w-text_w)/2:y=(h-text_h)/2:fontsize=50:fontcolor=red" -c:a copy -f rtsp rtsp://localhost:8554/compose-rtsp
# WORKS

#test with text and coordiantes: 
ffmpeg -re -stream_loop -1 -i footage-1-a.mp4 -vf "drawtext=text='Centered Text':x=10:y=10:fontsize=50:fontcolor=red" -c:a copy -f rtsp rtsp://localhost:8554/compose-rtsp
# WORKS


#test with show FPS on video AND reduce FPS! (Lower GPU load)
ffmpeg -i original.mp4 -vf "fps=25,drawtext=text='frame %{frame_num} %{pts}':x=10:y=10" output.mp4
ffmpeg -re -stream_loop -1 -i footage-1-a.mp4 -vf "fps=10,drawtext=text='frame %{frame_num} %{pts}':x=10:y=10:fontsize=50:fontcolor=red" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-a.mp4 -vf "fps=10" -pix_fmt yuv240p -f rtsp rtsp://localhost:8554/compose-rtsp


#Hardware Support
ffmpeg -hwaccel cuda -re -stream_loop -1 -i footage-1-a.mp4 -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp

#Reduce FPS with -r
ffmpeg -re -stream_loop -1 -r 10 -i footage-1-b.mp4 -vf -r 10 -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c copy -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c:v libx264 -preset slow -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c:v libx264 -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c:v libx264 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -c:v libx264 -f rtsp rtsp://localhost:8554/compose-rtsp


ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -c:v libx264 -max_interleave_delta 0 -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -c:v h264_nvenc -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -hwaccel cuvid -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -c:v h264_nvenc -f rtsp rtsp://localhost:8554/compose-rtsp

## New try in console (Only the RTSP Server in docker)
ffmpeg -re -stream_loop -1 -i footage-1-b.ts -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp

## Test with MJPEG as Input
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -vf "fps=5" -c:v libx264 -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -preset:v ultrafast -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -b:v 1M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -b:v 1M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -b:v 2M -maxrate 2M -bufsize 1M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -b:v 4M -maxrate 4M -bufsize 4M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -avoid_negative_ts make_zero -vcodec libx264 -b:v 4M -minrate 2M -maxrate 4M -bufsize 4M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -vcodec libx264 -b:v 4M -minrate 2M -maxrate 4M -bufsize 4M -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -vcodec libx264 -b:v 4M -minrate 2M -maxrate 4M -bufsize 4M -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -b:v 4M -minrate 2M -maxrate 10M -bufsize 4M -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -b:v 10M -minrate 1M -maxrate 10M -bufsize 4M -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg  -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4  -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c copy -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c copy -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vcodec libx264 -r 5 -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vcodec libx264 -r 5 -f rtsp rtsp://localhost:8554/compose-rtsp
-codec:v libx264 -crf 23

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -codec:v libx264 -crf 23 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -codec:v libx264 -crf 23 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -codec:v libx265 -crf 23 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -crf 23 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -codec:v rawvideo -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp

## The only working command without loosing quality: ##
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -c copy -f rtsp rtsp://localhost:8554/compose-rtsp

# test with MJPEG 
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -c copy -f rtsp rtsp://localhost:8554/compose-rtsp
# Error: [rtp @ 0x55afd5c76380] RFC 2435 requires standard Huffman tables for jpeg 1x   

# Basic command to reduce FPS (Bad quality result)
ffmpeg -re -stream_loop -1 -i footage-1-b.mjpeg -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5" -f rtsp rtsp://localhost:8554/compose-rtsp

# new commnad with bandwith - WORKS! Bandwith Parameter was missing for live transcoding.
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5"  -b:v 3M  -maxrate 3M -bufsize 1000K -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5"  -b:v 1M  -maxrate 2M -bufsize 1000K -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -vf "fps=5"  -b:v 1M  -maxrate 1M -bufsize 500K -f rtsp rtsp://localhost:8554/compose-rtsp

# 25 and 10 FPS works - 5 FPS still crashes 
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -codec:v libx264 -vf "fps=25" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -codec:v libx264 -preset medium -vf "fps=25" -f rtsp rtsp://localhost:8554/compose-rtsp
ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -codec:v libx264 -preset medium -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp

ffmpeg -re -stream_loop -1 -i footage-1-b.mp4 -codec:v mjpeg -huffman 0 -preset medium -vf "fps=10" -f rtsp rtsp://localhost:8554/compose-rtsp

# Convert to 5 FPS 
ffmpeg -i footage-1-b.mp4 -codec:v libx264 -vf "fps=5" footage-1-b_5FPS.mp4
ffmpeg -i footage-1-b.mp4 -filter:v fps=fps=5 footage-1-b_5FPS2.mp4

## Convert to ts
ffmpeg -i footage-1-b.mp4 -c copy footage-1-b.ts

# Start Stream:
start vlc rtsp://192.168.0.26:8554/compose-rtsp

