#!/bin/sh

FILE_BW=$1
BW=$2
if [ -f "$FILE_BW" ]; then
    echo "$FILE_BW exists."
else 
    clear
    echo "Fatal error"
    echo "File $FILE_BW does not exist."
    echo ""
    echo "Please start script with needed Videofile e.g."
    echo "################################################"
    echo "#  sh change-bandwith.sh footage-1-b.mp4 <BW>   #"
    echo "#  add '1' for 1 MBIT transcoding               #" 
    echo
    echo "################################################"
    echo ""
    echo "if you haven't downloaded the footage yet, download it first with WGET."
    echo ""
    exit
fi
    echo "$FILE_BW exists."
echo $BW

#Remove '*.mp4' fpr new filename
VIDEO_NAME=${FILE_BW%%.*}
echo $VIDEO_NAME
NAME_AND_BW="${VIDEO_NAME}_${BW}BW"
NAME_BW_AND_TEXT="${NAME_AND_BW}_Bandwith"

#docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg -y -vsync 0 -hwaccel cuda -hwaccel_output_format cuda -i "$FILE_BW" -g 10 -an -c:v h264_nvenc -b:v ${BW}M "$NAME_AND_BW".mp4
#docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg 
ffmpeg -i "$FILE_BW" -vf "drawtext=text='${BW}M':x=10:y=30:fontsize=20:fontcolor=red" -g 10 -an -b:v ${BW}M "$NAME_BW_AND_TEXT".mp4
rm -f "$NAME_AND_BW".mp4

ls -l -h *.mp4 *.MP4