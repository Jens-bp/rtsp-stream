#!/bin/bash
#echo $FILE
docker-compose down
echo $FILE
echo $FPS
FILE=$1
FPS=$2
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    clear
    echo "Fatal error"
    echo "File $FILE does not exist."
    echo ""
    echo "Please start script with needed Videofile e.g."
    echo "################################################"
    echo "#  sh stream-footage-1-a.sh footage-1-b.mp4    #" 
    echo "################################################"
    echo ""
    echo "if you haven't downloaded the footage yet, download it first with WGET."
    echo ""
    exit
fi
    echo "$FILE exists."
    echo "Exporting path"
export VIDEO_FILE=/video/$FILE
export FPS_COMPOSE=$FPS
echo $FPS
echo "################################################"
echo " Starting RTSP Stream for $FILE with $FPS FPS "
echo " RTSP Path                                    "
echo ""
echo " rtsp://<SYSTEM IP>:8554/compose-rtsp         "
echo "################################################"
docker-compose up -d