#!/bin/sh

FILE_FPS=$1
FPS=$2
if [ -f "$FILE_FPS" ]; then
    echo "$FILE_FPS exists."
else 
    clear
    echo "Fatal error"
    echo "File $FILE_FPS does not exist."
    echo ""
    echo "Please start script with needed Videofile e.g."
    echo "################################################"
    echo "#  sh change-fps.sh footage-1-b.mp4 <FPS>   #" 
    echo "################################################"
    echo ""
    echo "if you haven't downloaded the footage yet, download it first with WGET."
    echo ""
    exit
fi
    echo "$FILE_FPS exists."
echo $FPS

#Remove '*.mp4' fpr new filename
VIDEO_NAME=${FILE_FPS%%.*}
echo $VIDEO_NAME
NAME_AND_FPS="${VIDEO_NAME}_${FPS}FPS"
NAME_FPS_AND_TEXT="${NAME_AND_FPS}_Tx"

docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg -y -vsync 0 -hwaccel cuda -hwaccel_output_format cuda -i "$FILE_FPS" -g 10 -an -c:v h264_nvenc -b:v 2M  -filter:v fps=fps=${FPS} "$NAME_AND_FPS".mp4
#docker run --rm -it --runtime=nvidia --volume $PWD:/workspace willprice/nvidia-ffmpeg 
ffmpeg -i "$NAME_AND_FPS".mp4 -vf "drawtext=text='$NAME_AND_FPS':x=10:y=10:fontsize=40:fontcolor=red" -g 10 -an -b:v 2M "$NAME_FPS_AND_TEXT".mp4
rm -f "$NAME_AND_FPS".mp4

ls -l -h *.mp4 *.MP4